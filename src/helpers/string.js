


module.exports = {

    stripTags: (string) => String(string).replace(/(<([^>]+)>)/gi, ""),
}