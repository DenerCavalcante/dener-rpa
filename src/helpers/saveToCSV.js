
const jsonexport = require('jsonexport');
const fs = require('fs');


async function CSVToExport(data, filename){
     
    return new Promise( async( resolve, reject) => {

        const fileSync = fs.createWriteStream(`${filename}.csv`, {encoding: 'utf8'});
        jsonexport(data,   function(err, csv){
            if (err) return reject(err);
           
            fileSync.write(csv)
            fileSync.end()

            resolve(csv)
        });
    })

}



module.exports = CSVToExport