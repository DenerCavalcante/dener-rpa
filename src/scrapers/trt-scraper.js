"use strict"

const colors = require("colors");
const toCSV = require('../helpers/saveToCSV')
const helpers = require('../helpers/string')

const SEARCH_KEY = '"Acao trabalhista"';
const NUM_ITENS_PER_PAGE = 20;
const SOURCE_NAME = 'trt'

const objScraper = {
    url: "https://www.trt1.jus.br/web/guest/resultado-da-busca?p_p_id=3&p_p_lifecycle=0&p_p_state=maximized&p_p_mode=view&_3_groupId=0",
    async scraper(browser) {
        console.log(`scrapper ${this.url}`);

        const page = await browser.newPage();
        await page.goto(this.url);
        await page.type("#_3_keywords", SEARCH_KEY);


        page.click("#_3_search");

        await initialSelector();

        async function initialSelector() {

            try {
                await page.waitForSelector(".search-results", {
                    timeout: 120 * 1000,
                });
                let total = await page.$eval(".search-results", (e) => e.innerText);
                const url = await page.url();
                console.info(`✔ Resultados para pesquisa [${SEARCH_KEY}] : ${total}`);
                if (Number(total) == 0) browser.close();

                let i = 1;

                let items = await doPage(
                    `${url}`
                );

                while (await page.$(".pager lfr-pagination-buttons a")) {
                    const listOfData = await doPage(
                        `${url}&start=${i * NUM_ITENS_PER_PAGE}`
                    );
                    items = [...items, ...listOfData];
                    i += 1;
                }
                let listOfItems = [];
                for (let j in items) {
                    const item = items[j];
                    item.integra = await toPageComplete(item.link);
                    listOfItems.push(item);
                }
                console.log('add to csv...')
                await saveToCSV(listOfItems);
                browser.close();
            } catch (error) {
                console.log(error)
            }

        }

        async function doPage(url) {
            return new Promise(async(resolve) => {
                console.info(colors.bgGreen.white(`⚑ Pagina ${url} `));
                await page.goto(url);
                const resultsSelector = ".asset-entry";
                await page.waitForSelector(resultsSelector);
                const links = await page.evaluate((resultsSelector) => {
                    const items = Array.from(document.querySelectorAll(resultsSelector));
                    return items.map((item) => {
                        const anchor = item.querySelector(".asset-entry-title a");

                        const resume = String(item.querySelector("span").innerText).trim();
                        return {
                            link: anchor.href,
                            title: anchor.innerText,
                            resume,
                        };
                    });
                }, resultsSelector);
                resolve(links);
            });
        }

        async function toPageComplete(url) {
            return new Promise(async(resolve) => {
                try {
                    console.info(colors.bgCyan.white(`⚑ Recuperando detalhes - ${url} `));
                    await page.goto(url);
                    await page.waitForSelector(".journal-content-article", {
                        timeout: 20 * 1000,
                    });

                    const integra = await page.evaluate(
                        (e) => document.querySelector(".journal-content-article").innerText
                    );

                    resolve(integra);
                } catch (error) {
                    resolve(null);
                }
            });
        }

        async function saveToCSV(items) {
            return toCSV(
                items.map((row) => {
                    row.title = helpers.stripTags(row.title);
                    row.resume = String(row.resume).replace(/\s+/gi, " ");
                    row.integra = String(row.integra).replace(/\s+/gi, " ");
                    return row;
                }),
                `./csv/${SOURCE_NAME}`
            );
        }


    },
};

module.exports = objScraper;