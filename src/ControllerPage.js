async function run(brownserInstance, scraperInstance) {
    let browser;
    try {
        browser = await brownserInstance
        await scraperInstance.scraper(browser)

    } catch (error) {

    }
}

module.exports = (brownserInstance, scraperInstance) => run(brownserInstance, scraperInstance)